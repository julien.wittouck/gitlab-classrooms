package fr.univ_lille.gitlab.classrooms.assignments;

public enum AssignmentStatus {
    OPENED, ARCHIVED
}
